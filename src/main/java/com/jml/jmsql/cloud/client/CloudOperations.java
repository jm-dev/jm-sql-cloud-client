/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jml.jmsql.cloud.client;

import java.text.MessageFormat;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;

/**
 * Jersey REST client generated for REST resource:CloudOperations
 * [/cloud-operations]<br>
 * USAGE:
 * <pre>
 *        CloudOperations client = new CloudOperations();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author Michael L.R. Marques
 */
public class CloudOperations {
    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080//jmsql-cloud";

    public CloudOperations() {
        client = ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("cloud-operations");
    }

    public <T> T setDBXplora(Class<T> responseType, String username, String databases) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(MessageFormat.format("set-db-xplora/{0}/{1}", new Object[]{username, databases}));
        return resource.request(APPLICATION_XML).get(responseType);
    }

    public <T> T getDBXplora(Class<T> responseType, String username) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(MessageFormat.format("get-db-xplora/{0}", new Object[]{username}));
        return resource.request(APPLICATION_XML).get(responseType);
    }

    public void close() {
        client.close();
    }
    
}
